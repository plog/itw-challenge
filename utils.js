/** @module */

var endpointParsingRegex = /(\/[a-z0-9]+)+/;
var paramParsingRegex = /[a-z]+=\[[a-z ]+\]/;

/**
 * Extracts the endpoint from a text.
 * @param {string} str
 * @returns {string|null}
 */
exports.extractEndpoint = function(str) {
  var match = endpointParsingRegex.exec(str);
  return (Array.isArray(match) && match.length >= 1)? match[0]: null;
};

/**
 * Extracts the param to send as answer.
 * @param {string} str
 * @returns {string|null}
 */
exports.extractParam = function(str) {
  var param = null;
  var match = paramParsingRegex.exec(str);
  if (Array.isArray(match) && match.length >= 1) {
    param = match[0].split('=')[0];
  }
  return param;
};
