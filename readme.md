# Windowvation Ltd's Challenge

## Prerequisites:

* nodejs v0.10.x
* npm 1.4.x
* jshint v2.6.x (global)
* node-dev
* mocha
* git


## Installation

    $ brew install git
    $ brew install node # make sure to install version 0.10.x
    $ npm install -g node-dev
    $ npm install -g jshint
    $ npm install -g mocha
    $ npm install -g istanbul
    $ npm install # to be sure that all packages are here :)


## Coding Style

### Rules

- line lenght: 80
- indent: 2 spaces (no tabs)
- coding style: [Felix's Node.js Style Guide](https://github.com/felixge/node-style-guide)
- see `.jshintrc` and `.jscsrc`  for more specific rules
- see `documetnation/less.md` for more details about css

### Linting

    $ jshint

### Editor Tools

- vim: syntastic
- Sublime Text: Sublime Linter and SublimeLinter-jscs

## App Architecture

To Describe.

## Package management

### Node packages

We use [npm](https://npmjs.org/). Packages are listed in `package.json` file and ignored by git. Run `npm install` to install new packages (always use `--save` and `--save-dev` options to update the `package.json` file)

Be careful and run `npm install` to install new packages when you checkout.

## Unit tests

### Tools:

- mocha: test driver and test structure
- should: assertions/expectations
- sinon: spies, stubs and mocks
- proxyquire: module loading/stubbing

### Add tests:

Tests files are in the `tests` folder. Tests files name is `<file_to_test_name>_tests.js`.

### Run the tests:

    # run the tests
    $ npm test

### Code Coverage:

    # get the coverage
    $ ./scripts/coverage.sh

Then open `coverage/lcov-report/index.html` to get all the details.
