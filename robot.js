/** @module */

var util = require('util');

var client = require('./client');
var utils = require('./utils');
var Operation = require('./operation').Operation;

/**
 * Defines an Question Error.
 * @class RobotError
 */
var RobotError = function(msg) {
  Error.captureStackTrace(this, RobotError);
  this.message = msg || 'Error';
};
util.inherits(RobotError, Error);
RobotError.prototype.name = 'RobotError';

exports.RobotError = RobotError;

/**
 * Defines a robot with a name.
 * @class Robot
 * @param {string} name
 */
var Robot = function(name) {
  if (!name) {
    throw new RobotError('Robot must have a name.');
  }
  this.name = name;
  this.startingEndpoint = '/api';
  this.endpoint = null;
  this._supportedParams = ['name', 'answer'];
};

/**
 * Initializes the robot by answering the first question.
 * @param {function} cb
 */
Robot.prototype.initialize = function(cb) {
  var setRobotEndpoint = function(err, res, body) {
    if (err) {
      return cb(err);
    }
    var endpoint = utils.extractEndpoint(body);
    this.endpoint = endpoint;
    cb(err, res, body);
  };

  var answerQuestion = function(err, res, body) {
    if (err) {
      return cb(err);
    }
    this._sendAnswer(body, setRobotEndpoint.bind(this));
  };
  client.getQuestion(this.startingEndpoint, answerQuestion.bind(this));
};

/**
 * Answer a question.
 * @param {function} cb
 */
Robot.prototype.getAndAnswerQuestion = function(cb) {
  if (!this.endpoint) {
    return cb(new RobotError('Initialize the robot first.'));
  }
  var answerQuestion = function(err, res, body) {
    if (err) {
      return cb(err);
    }
    this._sendAnswer(body, cb);
  };
  client.getQuestion(this.endpoint, answerQuestion.bind(this));
};

/**
 * Extracts information from a question and sends the answer.
 * @param {string} str
 * @param {function} cb
 */
Robot.prototype._sendAnswer = function(str, cb) {
  var param = utils.extractParam(str);
  var endpoint = utils.extractEndpoint(str);
  if (!param) {
    return cb(new RobotError('Missing parameter to anwser question'));
  }

  if (this._supportedParams.indexOf(param) === -1) {
    return cb(new RobotError('Unsupported parameter'));
  }
  if (!endpoint) {
    return cb(new RobotError('Missing endpoint'));
  }
  var data = {};
  var value;
  if (param === 'name') {
    value = this.name;
  } else {
    try {
      var op = new Operation(str);
      value = op.compute();
    } catch(e) {
      return cb(new RobotError('Cannot process the operation.'));
    }
  }

  data[param] = value;
  client.answerQuestion(endpoint, data, cb);
};

exports.Robot = Robot;
