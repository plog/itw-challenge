/** @module */

var util = require('util');

var questionParsingRegex = /([0-9]+) (plus|minus|times) ([0-9]+)/;

/**
 * Defines an Question Error.
 * @class OperationError
 */
var OperationError = function(msg) {
  Error.captureStackTrace(this, OperationError);
  this.message = msg || 'Error';
};
util.inherits(OperationError, Error);
OperationError.prototype.name = 'OperationError';

/**
 * Defines an Operation.
 * An operation is created from a question that will be parsed
 * @class Operation
 * @param {string} str
 */
var Operation = function(str) {
  var terms = questionParsingRegex.exec(str);

  if (Array.isArray(terms) && terms.length >= 4) {
    terms = terms.splice(1, 3);
    // Better having proper ints than relying on duck typing. Would be
    // a problem with the addition anyway.
    terms[0] = parseInt(terms[0], 10);
    terms[2] = parseInt(terms[2], 10);
  } else {
    throw new exports.OperationError('Not a valid question.');
  }
  this.kind = terms[1];
  this.t1 = terms[0];
  this.t2 = terms[2];
};

/**
 * Computest the operation results.
 * We expect that the operation object has not been mdified and that t1 and t2
 * are ints.
 * @return {int}
 */
Operation.prototype.compute = function() {
  var res;
  switch (this.kind) {
    case 'plus':
      res = this.t1 + this.t2;
      break;
    case 'minus':
      res = this.t1 - this.t2;
      break;
    case 'times':
      res = this.t1 * this.t2;
      break;
    default:
      throw new OperationError('Not a valid operation.');
  }
  return res;
};

exports.OperationError = OperationError;
exports.Operation = Operation;
