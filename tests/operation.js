/* global describe: true, it: true */
/* jshint expr: true */
var should = require('should');

var Operation = require('../operation.js').Operation;
var OperationError = require('../operation.js').OperationError;

describe('Operation', function() {
  it('should create an Operation from a question', function() {
    var str = 'What is 90 plus 56?\nPOST answer=[answer] to /user/125/answer\n';
    var op = new Operation(str);
    op.kind.should.equal('plus');
    op.t1.should.equal(90);
    op.t2.should.equal(56);

    str = 'What is 100 minus 42?\nPOST answer=[answer] to /user/1025/answer\n';
    op = new Operation(str);
    op.kind.should.equal('minus');
    op.t1.should.equal(100);
    op.t2.should.equal(42);

    str = 'What is 100 times 42?\nPOST answer=[answer] to /user/1025/answer\n';
    op = new Operation(str);
    op.kind.should.equal('times');
    op.t1.should.equal(100);
    op.t2.should.equal(42);

    str = 'What is 100 pluss 42?\nPOST answer=[answer] to /user/1025/answer\n';

    /* jshint unused: false */
    should.throws(
      function() { var op = new Operation(str); },
      OperationError,
      'Not a valid question.'
    );
    /* jshint unused: true */
  });

  it('should create an operation from a question', function() {
    var str = 'What is 90 plus 56?\nPOST answer=[answer] to /user/125/answer\n';
    var op = new Operation(str);
    op.compute().should.equal(op.t1 + op.t2);
    op.compute().should.equal(146);

    str = 'What is 100 minus 42?\nPOST answer=[answer] to /user/1025/answer\n';
    op = new Operation(str);
    op.compute().should.equal(op.t1 - op.t2);
    op.compute().should.equal(58);

    str = 'What is 100 times 42?\nPOST answer=[answer] to /user/1025/answer\n';
    op = new Operation(str);
    op.compute().should.equal(op.t1 * op.t2);
    op.compute().should.equal(4200);

    op.kind = 'foo';
    should.throws(
      op.compute,
      OperationError,
      'Not a valid operation.'
    );
  });
});
