/* global describe: true, it: true */
/* jshint expr: true */
var should = require('should');

var utils = require('../utils.js');

describe('Utils', function() {
  it('should extract endpoints from body', function() {
    var body = 'Hello John!\nGET the first question from /user/1019/question\n';
    var expected = '/user/1019/question';
    var endpoint = utils.extractEndpoint(body);
    endpoint.should.equal(expected);

    body = 'Hello John!\nGET the first /user/1019/question ' +
      'question from /user/1019/question\n';
    endpoint = utils.extractEndpoint(body);
    endpoint.should.equal(expected);

    body = 'Hello John!\nGET the first question from /user/1019\n';
    expected = '/user/1019';
    endpoint = utils.extractEndpoint(body);
    endpoint.should.equal(expected);

    body = 'Hello John!\nGET the first question from /user\n';
    expected = '/user';
    endpoint = utils.extractEndpoint(body);
    endpoint.should.equal(expected);

    body = 'Hello John!\nGET the first question from \n';
    endpoint = utils.extractEndpoint(body);
    should(endpoint).be.null;
  });

  it('should extract params from body', function() {
    var body = 'What is 95 minus 37?\nPOST answer=[answer] to /user/1025';
    var expected = 'answer';
    var param = utils.extractParam(body);
    param.should.equal(expected);

    body = 'What is 95 minus 37?\nPOST foo=[barr] to /user/1025';
    expected = 'foo';
    param = utils.extractParam(body);
    param.should.equal(expected);

    body = 'Hello John!\nGET the first question from /user/1019\n';
    param = utils.extractParam(body);
    should(param).be.null;
  });
});
