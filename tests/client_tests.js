/* global describe: true, it: true, beforeEach: true */
/* jshint expr: true */
require('should');
var proxyquire = require('proxyquire');
var sinon = require('sinon');

var ClientError = require('../client').ClientError;

describe('Client', function() {
  beforeEach(function() {
    this.requestStub = sinon.stub();
    this.client = proxyquire('../client', { request: this.requestStub });
  });

  describe('getQuestion', function() {
    it('should get a question from an endpoint', function(done) {
      var expected = {
        method: 'GET',
        uri: this.client.API_BASE_URL + 'foo',
      };
      var check = function() {
        this.requestStub.calledOnce.should.be.true;
        this.requestStub.calledWithExactly([expected]);
        done();
      };
      this.requestStub.callsArgWithAsync(1, null, { statusCode: 200 }, 'foo');
      this.client.getQuestion('foo', check.bind(this));
    });

    it('should get a question from an endpoint - req error', function(done) {
      var expected = {
        method: 'GET',
        uri: this.client.API_BASE_URL + 'foo',
      };
      var check = function(err) {
        err.should.eql(new Error());
        this.requestStub.calledOnce.should.be.true;
        this.requestStub.calledWithExactly([expected]);
        done();
      };
      this.requestStub.callsArgWithAsync(1, new Error());
      this.client.getQuestion('foo', check.bind(this));
    });

    it('should get a question from an endpoint - bad status', function(done) {
      var expected = {
        method: 'GET',
        uri: this.client.API_BASE_URL + 'foo',
      };
      var check = function(err) {
        err.should.eql(new ClientError('Request error'));
        this.requestStub.calledOnce.should.be.true;
        this.requestStub.calledWithExactly([expected]);
        done();
      };
      this.requestStub.callsArgWithAsync(1, null, { statusCode: 400 }, 'bar');
      this.client.getQuestion('foo', check.bind(this));
    });
  });

  describe('answerQuestion', function() {
    it('should post the answer to a question', function(done) {
      var data = { foo: 'bar' };
      var expected = {
        method: 'POST',
        uri: this.client.API_BASE_URL + 'foo',
        form: data,
      };
      var check = function() {
        this.requestStub.calledOnce.should.be.true;
        this.requestStub.calledWithExactly([expected]);
        done();
      };
      this.requestStub.callsArgWithAsync(1, null, { statusCode: 200 }, 'foo');
      this.client.answerQuestion('foo', data, check.bind(this));
    });

    it('should post the answer to a question - request error', function(done) {
      var data = { foo: 'bar' };
      var expected = {
        method: 'POST',
        uri: this.client.API_BASE_URL + 'foo',
        form: data,
      };
      var check = function(err) {
        err.should.eql(new Error());
        this.requestStub.calledOnce.should.be.true;
        this.requestStub.calledWithExactly([expected]);
        done();
      };
      this.requestStub.callsArgWithAsync(1, new Error());
      this.client.answerQuestion('foo', data, check.bind(this));
    });

    it('should post the answer to a question - bad status', function(done) {
      var data = { foo: 'bar' };
      var expected = {
        method: 'POST',
        uri: this.client.API_BASE_URL + 'foo',
        form: data,
      };
      var check = function(err) {
        err.should.eql(new ClientError('Request error'));
        this.requestStub.calledOnce.should.be.true;
        this.requestStub.calledWithExactly([expected]);
        done();
      };
      this.requestStub.callsArgWithAsync(1, null, { statusCode: 500 }, 'bar');
      this.client.answerQuestion('foo', data, check.bind(this));
    });

    it('should post the answer to a question - bad answer', function(done) {
      var data = { foo: 'bar' };
      var expected = {
        method: 'POST',
        uri: this.client.API_BASE_URL + 'foo',
        form: data,
      };
      var check = function(err) {
        err.should.eql(new ClientError('Bad answer'));
        this.requestStub.calledOnce.should.be.true;
        this.requestStub.calledWithExactly([expected]);
        done();
      };
      this.requestStub.callsArgWithAsync(1, null, { statusCode: 400 }, 'bar');
      this.client.answerQuestion('foo', data, check.bind(this));
    });
  });
});
