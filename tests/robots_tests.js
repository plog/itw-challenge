/* global describe: true, it: true, beforeEach: true */
/* jshint expr: true */
var should = require('should');
var sinon = require('sinon');

var client = require('../client');
var Robot = require('../robot').Robot;
var RobotError = require('../robot').RobotError;

describe('Robot', function() {
  beforeEach(function() {
    this.robotName = 'HW';
    this.robot = new Robot(this.robotName);
  });

  describe('Constructor', function() {
    it('should get create a Robot instance', function() {
      var name = 'John';
      var robot = new Robot(name);

      robot.name.should.equal(name);
      robot.startingEndpoint.should.equal('/api');
      robot._supportedParams.should.eql(['name', 'answer']);
      should(robot.endpoint).be.null;

      should.throws(
        function() { return new Robot(); },
        RobotError,
        'Robot must have a name.'
      );
    });
  });

  describe('initialize', function() {
    it('should answer the name question', function(done) {
      var text = 'What is your name?\nPOST name=[your name] ' +
        '(form encoded) to /begin';
      var getQuestionStub = sinon.stub(client, 'getQuestion');
      getQuestionStub.callsArgWithAsync(1, null, { statusCode: 200 }, text);

      var text2 = 'Hello John!\nGET the first /user/1019/question ' +
      'question from /user/1019/question\n';
      var answerQuestionStub = sinon.stub(client, 'answerQuestion');
      answerQuestionStub.callsArgWithAsync(2, null, { statusCode: 200 }, text2);

      var check = function(err, resp, body) {
        getQuestionStub.calledOnce.should.be.true;
        getQuestionStub.args[0][0].should.eql('/api');
        answerQuestionStub.calledOnce.should.be.true;
        answerQuestionStub.args[0][0].should.eql('/begin');
        this.robot.endpoint.should.equal('/user/1019/question');
        body.should.equal(text2);
        getQuestionStub.restore();
        answerQuestionStub.restore();
        done();
      };

      this.robot.initialize(check.bind(this));
    });
  });

  describe('getAndAnswerQuestion', function() {
    it('should answer a math question', function(done) {
      should.throws(this.robot.getAndAnswerQuestion);

      this.robot.endpoint = '/user/1025/question';
      var text = 'What is 95 minus 37?\n' +
        'POST answer=[answer] to /user/1025/answer';
      var getQuestionStub = sinon.stub(client, 'getQuestion');
      getQuestionStub.callsArgWithAsync(1, null, { statusCode: 200 }, text);

      var text2 = 'Correct!\n' +
        'GET the next question from /user/1025/question';
      var answerQuestionStub = sinon.stub(client, 'answerQuestion');
      answerQuestionStub.callsArgWithAsync(2, null, { statusCode: 200 }, text2);

      var check = function(err, resp, body) {
        getQuestionStub.calledOnce.should.be.true;
        getQuestionStub.args[0][0].should.eql('/user/1025/question');
        answerQuestionStub.calledOnce.should.be.true;
        answerQuestionStub.args[0][0].should.eql('/user/1025/answer');
        body.should.equal(text2);
        getQuestionStub.restore();
        answerQuestionStub.restore();
        done();
      };

      this.robot.getAndAnswerQuestion(check.bind(this));
    });
  });
});
