var Robot = require('./robot').Robot;

var nbQuestions = 10;
var nbRemainingQuestions = nbQuestions;

var robot = new Robot('John');
var nextQuestion = function(err) {
  if (err) {
    console.error(err, nbRemainingQuestions);
    return;
  }
  --nbRemainingQuestions;
  if (nbRemainingQuestions > -1) {
    robot.getAndAnswerQuestion(nextQuestion);
  } else {
    console.log('Done!');
  }
};

robot.initialize(nextQuestion);
