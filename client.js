/** @module */

var request = require('request');
var util = require('util');

exports.API_BASE_URL = 'http://windowvationquizserver.elasticbeanstalk.com';

/**
 * Defines an Question Error.
 * @class ClientError
 */
var ClientError = function(msg) {
  Error.captureStackTrace(this, ClientError);
  this.message = msg || 'Error';
};
util.inherits(ClientError, Error);
ClientError.prototype.name = 'ClientError';

exports.ClientError = ClientError;

/**
 * Sends the answer to a question.
 * @param {string} endpoint
 * @param {object} data
 * @param {function} cb
 */
exports.answerQuestion = function(endpoint, data, cb) {
  request({
    method: 'POST',
    uri: exports.API_BASE_URL + endpoint,
    form: data
  }, function(err, res, body) {
    if (err) {
      return cb(err);
    } else if (res.statusCode === 400) {
      return cb(new ClientError('Bad answer'));
    } else if (res.statusCode !== 200) {
      return cb(new ClientError('Request error'));
    }
    cb(err, res, body);
  });
};

/**
 * Gets the question.
 * @param {string} endpoint
 * @param {function} cb
 */
exports.getQuestion = function(endpoint, cb) {
  request({
    method: 'GET',
    uri: exports.API_BASE_URL + endpoint,
  }, function(err, res, body) {
    if (err) {
      return cb(err);
    } else if (res.statusCode !== 200) {
      return cb(new ClientError('Request error'));
    }
    cb(err, res, body);
  });
};
